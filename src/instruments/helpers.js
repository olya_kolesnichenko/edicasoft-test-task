export function getFilteredUsers (users, filter) {
    return filter !== ""
        ? users.filter((user) => (user.username.toLowerCase()).includes(filter.toLowerCase()))
        : users;
}
