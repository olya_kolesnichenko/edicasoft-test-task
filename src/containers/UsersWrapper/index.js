import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
// Instruments
import { getFilteredUsers } from "instruments/helpers";

import { FaUserPlus, FaSearch } from "react-icons/fa";
import { usersActions as actions } from "bus/users/actions";

import UsersList from 'components/UsersList';
import UserForm from 'components/UserForm';
import Spinner from 'components/Spinner';

import styles from './styles.module.scss';

class UsersWrapper extends Component {
    state = {
        error:      null,
        isOpenForm: false,
        filter: ''
    };

    componentDidMount () {
        this.props.actions.fetchUsersAsync();
    }

    handleOpenForm = () => {
        this.setState({isOpenForm: !this.state.isOpenForm})
    };

    handleSearch= (e) => {
        this.setState({filter: e.target.value});
    };

    render () {

        const { users, isFetching, actions: {
            removeUserAsync,
            updateUserAsync,
            createUserAsync,
            setEditingUser
        }} = this.props;

        const { isOpenForm, filter } = this.state;

        let filteredUsersList = getFilteredUsers(users, filter);

        let content = (!isFetching && filteredUsersList.length) ?
            <UsersList
                users={ filteredUsersList }
                removeUser = { removeUserAsync }
                handleOpenForm = { this.handleOpenForm }
                setEditingUser = { setEditingUser }
            /> : <p>No results</p>;

        return (
            (isFetching ? <Spinner/> :
                    <section className={styles.container}>
                        <button className={styles.buttonAdd} onClick={ this.handleOpenForm }><FaUserPlus /> <span>Add user</span></button>
                        <div className={styles.searchWrapper}>
                            <FaSearch/>
                            <input
                            type="search"
                            placeholder="Search by username"
                            onKeyUp={this.handleSearch}
                        />
                        </div>
                        { content }
                        {isOpenForm && <UserForm
                            handleOpenForm={ this.handleOpenForm }
                            createUser={ createUserAsync }
                            updateUser={ updateUserAsync }
                            setEditingUser = { setEditingUser }
                        />}
                    </section>)
        );
    }
}

const mapStateToProps = ( state ) => ({
    users:	state.users.users,
    isFetching:	state.users.isFetching
});

const mapDispatchToProps = ( dispatch ) => ({
    actions: bindActionCreators({...actions}, dispatch)
});

export default connect(mapStateToProps, mapDispatchToProps)(UsersWrapper) ;