//Core
import { put, apply } from "redux-saga/effects";

// Instruments
import { usersActions } from "../../actions";
import { api } from "./../../../../REST/api";


export function* fetchUsers () {
    try {
        yield put(usersActions.startFetching());
        const response = yield apply(api, api.users.fetch);

        if (response.status !== 200) {
            throw new Error("Failed to load data");
        }
        const data = yield apply(response, response.json);

        yield put(usersActions.fillUsers(data));
    } catch (error) {
        console.log(error)
    } finally {
        yield put(usersActions.stopFetching());
    }
}
