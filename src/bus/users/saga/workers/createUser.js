//Core
import { put, apply } from "redux-saga/effects";

// Instruments
import { usersActions } from "../../actions";
import { api } from "./../../../../REST/api";


export function* createUser ({ payload: user }) {
    try {
        yield put(usersActions.startFetching());
        const response = yield apply(api, api.users.create, [user]);

        if (response.status !== 201) {
            throw new Error("Failed to create");
        }
        const data = yield apply(response, response.json);

        yield put(usersActions.createUser(data));
    } catch (error) {
        console.log(error)
    } finally {
        yield put(usersActions.stopFetching());
    }
}
