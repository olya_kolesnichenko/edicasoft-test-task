//Core
import { put, apply } from "redux-saga/effects";

// Instruments
import { usersActions } from "../../actions";
import { api } from "./../../../../REST/api";


export function* updateUser ({ payload: user }) {
    try {
        yield put(usersActions.startFetching());
        const response = yield apply(api, api.users.update, [user]);

        //We need to ignore errors because of mocked API !!!

        const data = yield apply(response, response.json);

        yield put(usersActions.updateUser(data));
    } catch (error) {
        console.log(error)
    } finally {
        yield put(usersActions.stopFetching());
    }
}
