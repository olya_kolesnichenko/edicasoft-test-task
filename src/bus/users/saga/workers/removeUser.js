//Core
import { put, apply } from "redux-saga/effects";

// Instruments
import { usersActions } from "../../actions";
import { api } from "./../../../../REST/api";


export function* removeUser ({ payload: userId }) {
    try {
        yield put(usersActions.startFetching());
        const response = yield apply(api, api.users.remove, [userId]);

        if (response.status !== 200) {
            throw new Error("Failed to remove");
        }

        yield put(usersActions.removeUser(userId));
    } catch (error) {
        console.log(error)
    } finally {
        yield put(usersActions.stopFetching());
    }
}
