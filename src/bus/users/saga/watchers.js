//Core
import { takeEvery, all, call } from "redux-saga/effects";

//Types
import { types } from "../types";

//Workers
import { fetchUsers, removeUser, createUser, updateUser } from "./workers";

function* watchFetchUsers () {
    yield takeEvery(types.FETCH_USERS_ASYNC, fetchUsers);
}

function* watchRemoveUser () {
    yield takeEvery(types.REMOVE_USER_ASYNC, removeUser);
}

function* watchCreateUser () {
    yield takeEvery(types.CREATE_USER_ASYNC, createUser);
}

function* watchUpdateUser () {
    yield takeEvery(types.UPDATE_USER_ASYNC, updateUser);
}

export function* watchUsers () {
    yield all([call(watchFetchUsers), call(watchRemoveUser), call(watchCreateUser), call(watchUpdateUser)]);
}
