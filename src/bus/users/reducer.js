//Types
import { types } from "./types";

const initialState = {
    users: [],
    isFetching: false,
    editingUser: null
};

export const usersReducer = (state = initialState, action) => {
    switch (action.type) {
        case types.START_FETCHING:
            return {
                ...state,
                isFetching: true
            };
        case types.STOP_FETCHING:
            return {
                ...state,
                isFetching: false
            };
        case types.FILL_USERS:
            return {
                ...state,
                users: action.payload
            };
        case types.REMOVE_USER:
            return {
                ...state,
                users: state.users.filter((user) => user.id !== action.payload)
            };
        case types.CREATE_USER:
            return {
                ...state,
                users: [...state.users, action.payload]
            };
        case types.SET_EDITING_USER:
            return {
                ...state,
                editingUser: action.payload
            };
        case types.UPDATE_USER:
            return {
                ...state,
                users: state.users.map((user) => {
                    if(user.id === action.payload.id)
                        user = {...user, ...action.payload};
                    return user
                })
            };
        default:
            return state;
    }
};