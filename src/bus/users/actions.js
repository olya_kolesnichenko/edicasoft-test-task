//Types
import { types } from "./types";

export const usersActions = {
    //Sync
    fillUsers: (users) => {
        return {
            type:    types.FILL_USERS,
            payload: users,
        };
    },
    startFetching: () => {
        return {
            type: types.START_FETCHING,
        };
    },
    stopFetching: () => {
        return {
            type: types.STOP_FETCHING,
        };
    },
    removeUser: (userId) => {
        return {
            type: types.REMOVE_USER,
            payload: userId
        };
    },
    createUser: (user) => {
        return {
            type: types.CREATE_USER,
            payload: user
        };
    },
    setEditingUser: (user) => {
        return {
            type: types.SET_EDITING_USER,
            payload: user
        };
    },
    updateUser: (user) => {
        return {
            type: types.UPDATE_USER,
            payload: user
        };
    },
    //Async
    fetchUsersAsync: () => {
        return {
            type: types.FETCH_USERS_ASYNC,
        };
    },
    removeUserAsync: (userId) => {
        return {
            type: types.REMOVE_USER_ASYNC,
            payload: userId
        };
    },
    createUserAsync: (user) => {
        return {
            type: types.CREATE_USER_ASYNC,
            payload: user
        };
    },
    updateUserAsync: (user) => {
        return {
            type: types.UPDATE_USER_ASYNC,
            payload: user
        };
    },
};
