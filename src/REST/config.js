const ROOT_URL = "https://jsonplaceholder.typicode.com";
const MAIN_URL = `${ROOT_URL}/users`;

export { ROOT_URL, MAIN_URL };
