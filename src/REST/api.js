//Instruments
import { MAIN_URL } from "./config";

export const api = {
    users: {
        fetch () {
            return fetch(`${MAIN_URL}/`, {
                method:  "GET",
            });
        },
        create (user) {
            return fetch(`${MAIN_URL}/`, {
                method:  "POST",
                headers: {
                    "Content-type": "application/json; charset=UTF-8"
                },
                body: JSON.stringify(user),
            });
        },
        remove (userId) {
            return fetch(`${MAIN_URL}/${userId}`, {
                method:  "DELETE"
            });
        },
        update (user) {
            return fetch(`${MAIN_URL}/${user.id}`, {
                method:  "PUT",
                headers: {
                    "Content-type": "application/json; charset=UTF-8"
                },
                body: JSON.stringify(user),
            });
        },
    },
};
