//Core
import { all, call } from "redux-saga/effects";

//Watchers
import { watchUsers } from "bus/users/saga/watchers";

export function* rootSaga () {
    yield all([call(watchUsers)]);
}
