import { createStore, applyMiddleware, compose } from 'redux';
import createSagaMiddleware from 'redux-saga';

//Roots
import { rootReducer } from "./rootReducer";
import { rootSaga } from "./rootSaga";
import { createLogger } from "redux-logger";

const logger = createLogger({
    duration:  true,
    collapsed: true,
    colors:    {
        title:     () => "#139BFE",
        prevstate: () => "#1C5FAF",
        action:    () => "#149945",
        nextState: () => "#A47104",
        error:     () => "#FF0005",
    },
});

const dev = process.env.NODE_ENV === 'development';
const devtools = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__;
const composeEnhancers = devtools && dev ? devtools : compose;
const sagaMiddleware = createSagaMiddleware();

const middleware = [sagaMiddleware];

if (dev) {
    middleware.push(logger);
}

export default createStore(
    rootReducer,
    composeEnhancers(applyMiddleware(...middleware))
);

sagaMiddleware.run(rootSaga);