//Core
import { combineReducers } from "redux";

//Reducers
import { usersReducer as users } from "bus/users/reducer";

export const rootReducer = combineReducers({ users });
