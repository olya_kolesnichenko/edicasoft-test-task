import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';

import store from './init/store';

import UsersWrapper from 'containers/UsersWrapper';

ReactDOM.render(
    <Provider store={ store }>
        <UsersWrapper />
    </Provider>,
  document.getElementById('root')
);

