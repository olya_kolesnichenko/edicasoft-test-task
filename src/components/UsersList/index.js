import React from 'react';

import Styles from './styles.module.scss';
import User from '../User';

const UsersList = ({ users, removeUser, editUser, handleOpenForm, setEditingUser }) => {

    const usersList = users.map( user => {

        return (
            <User
                key={ user.id }
                user = {user}
                removeUser={ removeUser }
                editUser={ editUser}
                handleOpenForm = { handleOpenForm }
                setEditingUser = { setEditingUser }
            />
        );
    });

    return (
        <div className={ Styles.users }>
            { usersList }
        </div>
    );
}

export default UsersList;