import React, {Component} from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

import { MdClose } from "react-icons/md";
import { usersActions as actions } from "bus/users/actions";

import styles from './styles.module.scss';

class UserForm extends Component {
    constructor(props) {
        super(props);
        const { editingUser } = this.props;
        this.state = {
            username: editingUser ? editingUser.username : '',
            email: editingUser ? editingUser.email : '',
            address: (editingUser && editingUser.address.street ) ? editingUser.address.street : '',
            errors: {
                username: '',
                email: '',
            }
        };
    }

    validateForm = (errors) => {
        let valid = true;
        Object.values(errors).forEach(
            (val) => val.length > 0 && (valid = false)
        );
        return valid;
    };

    handleChange = (event) => {
        event.preventDefault();
        this.validateField(event.target);
    };

    validateField = (target) => {
        const {name, value} = target;
        let errors = this.state.errors;

        // eslint-disable-next-line
        const validEmailRegex = RegExp(/^(([^<>()\[\]\.,;:\s@\"]+(\.[^<>()\[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i);

        switch (name) {
            case 'username':
                errors.username =
                    value.length < 3
                        ? 'Username must be 3 characters long!'
                        : '';
                break;
            case 'email':
                errors.email =
                    validEmailRegex.test(value)
                        ? ''
                        : 'Email is not valid!';
                break;
            default:
                break;
        }

        this.setState({errors, [name]: value});
    };

    handleClose = () => {
        const { handleOpenForm, editingUser, setEditingUser  } = this.props;
        handleOpenForm();

        if (editingUser){
            setEditingUser(null);
        }
    };
    handleSubmit = (event) => {
        event.preventDefault();

        let formElements = event.target.elements;
        for (let i = 0; i < formElements.length; i++) {
            if (formElements[i].nodeName === "INPUT") {
                this.validateField(formElements[i]);
            }
        }
        if (this.validateForm(this.state.errors)) {
            const { handleOpenForm, createUser, editingUser, updateUser, setEditingUser  } = this.props;
            const { username, email, address } = this.state;

            handleOpenForm();

            if (editingUser){
                updateUser({...editingUser, username, email, address: {street: address}});
                setEditingUser(null);
            } else {
                createUser({username, email, address: {street: address}});
            }
        }
    };

    render() {
        const {errors, username, email, address } = this.state;
        const { editingUser } = this.props;

        return (
            <div className={styles.formWrapper}>
                <div className={styles.form}>
                    <h2>{editingUser ? 'Edit User' : 'Create User'}</h2>
                    <span className={styles.close} onClick={this.handleClose}><MdClose/></span>
                    <form onSubmit={this.handleSubmit} noValidate>
                        <div className={styles.formField}>
                            <label htmlFor="username">Userame</label>
                            <input type='text' name='username' onChange={this.handleChange} value={username} noValidate/>
                            {errors.username.length > 0 &&
                            <span className={styles.error}>{errors.username}</span>}
                        </div>
                        <div className={styles.formField}>
                            <label htmlFor="email">Email</label>
                            <input type='email' name='email' onChange={this.handleChange} value={email} noValidate/>
                            {errors.email.length > 0 &&
                            <span className={styles.error}>{errors.email}</span>}
                        </div>
                        <div className={styles.formField}>
                            <label htmlFor="address">Address</label>
                            <input type='text' name='address' onChange={this.handleChange} value={address} noValidate/>
                        </div>
                        <div className={styles.buttonWrapper}>
                            <button >{editingUser ? 'Edit' : 'Create'}</button>
                        </div>
                    </form>
                </div>
            </div>
        );
    }
}

const mapStateToProps = ( state ) => ({
    editingUser:	state.users.editingUser,
    isFetching:	state.users.isFetching
});

const mapDispatchToProps = ( dispatch ) => ({
    actions: bindActionCreators({...actions}, dispatch)
});

export default connect(mapStateToProps, mapDispatchToProps)(UserForm) ;