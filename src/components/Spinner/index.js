// Core
import React from "react";

// Instruments
import styles from "./styles.module.scss";

const Spinner = () => {
    return (
        <div className = { styles.spinner } />
    );
};

export default Spinner;