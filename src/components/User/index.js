import React  from 'react';

import { MdDelete, MdEdit } from "react-icons/md";

import styles from './styles.module.scss';

const User = ({
                  removeUser,
                  handleOpenForm,
                  setEditingUser,
                  user
            }) => {


    const handleDelete = (e) => { removeUser(user.id) };

    const handleEdit = (e) => {

        setEditingUser(user);
        handleOpenForm();
    };


    return (
        <div className={styles.userItem}>
            <div className={styles.userInfo}>
                <p className={styles.userName}>{user.username}</p>
                <p className={styles.userEmail}>{user.email}</p>
                {(user.address && user.address.street) ? <p>Live on <b>{user.address.street}</b> street</p> : null}
            </div>
            <div className={styles.buttons}>
                <span className={styles.buttonEdit} onClick={ handleEdit }><MdEdit/></span>
                <span className={styles.buttonDelete} onClick={ handleDelete }><MdDelete/></span>
            </div>
        </div>
    );
}


export default User;